package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	v1 "gitlab.com/medium_app/blog_app_api_gateway/api/v1"
	"gitlab.com/medium_app/blog_app_api_gateway/config"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	grpcPkg "gitlab.com/medium_app/blog_app_api_gateway/pkg/grpc_client"

	_ "gitlab.com/medium_app/blog_app_api_gateway/api/docs" // for swagger
)

type RouterOptions struct {
	Cfg        *config.Config
	GrpcClient grpcPkg.GrpcClientI
	Logger     *logrus.Logger
}

// @title           Swagger for blog api
// @version         1.0
// @description     This is a blog service api.
// @BasePath  /v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:        opt.Cfg,
		GrpcClient: opt.GrpcClient,
		Logger:     opt.Logger,
	})

	apiV1 := router.Group("/v1")

	apiV1.POST("/auth/register", handlerV1.Register)
	apiV1.POST("/auth/verify", handlerV1.Verify)
	apiV1.POST("/auth/login", handlerV1.Login)
	apiV1.POST("/auth/forgot-password", handlerV1.ForgotPassword)
	apiV1.POST("/auth/verify-forgot-password", handlerV1.VerifyForgotPassword)
	apiV1.POST("/auth/update-password", handlerV1.AuthMiddleware("users", "update-password"), handlerV1.UpdatePassword)

	apiV1.GET("/users/:id", handlerV1.GetUser)
	apiV1.GET("/users/me", handlerV1.AuthMiddleware("users", "get-user-profile"), handlerV1.GetUserProfile)
	apiV1.GET("/users", handlerV1.GetUsers)
	apiV1.GET("/users/email/:email", handlerV1.GetUserByEmail)
	apiV1.POST("/users", handlerV1.AuthMiddleware("users", "create"), handlerV1.CreateUser)
	apiV1.PUT("/users/:id", handlerV1.AuthMiddleware("users", "update"), handlerV1.UpdateUser)
	apiV1.DELETE("users/:id", handlerV1.AuthMiddleware("users", "delete"), handlerV1.DeleteUser)

	apiV1.GET("/posts/:id", handlerV1.GetPost)
	apiV1.GET("/posts", handlerV1.GetPosts)
	apiV1.POST("/posts", handlerV1.AuthMiddleware("posts", "create"), handlerV1.CreatePost)
	apiV1.PUT("/posts/:id", handlerV1.AuthMiddleware("posts", "update"), handlerV1.UpdatePost)
	apiV1.DELETE("posts/:id", handlerV1.DeletePost)

	apiV1.GET("/categories/:id", handlerV1.GetCategory)
	apiV1.GET("/categories", handlerV1.GetCategories)
	apiV1.POST("/categories", handlerV1.AuthMiddleware("categories", "create"), handlerV1.CreateCategory)
	apiV1.PUT("/categories/:id", handlerV1.UpdateCategory)
	apiV1.DELETE("categories/:id", handlerV1.DeleteCategory)

	apiV1.GET("/comments", handlerV1.GetComments)
	apiV1.POST("/comments", handlerV1.AuthMiddleware("comments", "create"), handlerV1.CreateComment)
	apiV1.PUT("/comments/:id", handlerV1.UpdateComment)
	apiV1.DELETE("comments/:id", handlerV1.DeleteComment)

	apiV1.GET("/likes/user-post", handlerV1.AuthMiddleware("likes", "get"), handlerV1.GetLike)
	apiV1.POST("/likes", handlerV1.AuthMiddleware("likes", "create-or-update"), handlerV1.CreateOrUpdateLike)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
